#!/usr/bin/env node

const prompt = require('prompt');
prompt.start();
prompt.get(['id', 'token'], (err, result) => {
  if (!err) {
    const access = Buffer.from(`${result.id}:${result.token}`).toString('base64')
    console.log('')
    console.log(access)
  } 
});